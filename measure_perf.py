# -*- coding: utf-8 -*-

import gc
import random
import time

import numpy as np
import ujson

from numpy_based_dict_of_dict import DictOfDict
from numpy_based_dict_of_dict2 import ImmutableDictOfDict
from util import get_memory_usage
import memory_profiling

JSON_FILE = './data/1.json'


def prepare_true_key_rows(dct, n=10):
    questions = []
    keys = dct.keys()
    n_key = len(keys)
    for i in range(n):
        key_idx = int(random.random() * n_key)
        questions.append(keys[key_idx])
    return questions


def prepare_false_key_rows(dct, n=10):
    questions = []
    keys = dct.keys()
    n_key = len(keys)
    for i in range(n):
        key_idx = int(random.random() * n_key)
        q = keys[key_idx] + 'z'
        while q in keys:
            q += 'z'
        questions.append(q)
    return questions


def timeit(number=10):
    def _deco(func):
        def _wrapper(*args, **kwargs):
            total_time = 0
            for i in range(number):
                start_time = time.time()
                r = func(*args, **kwargs)
                end_time = time.time()
                total_time += (end_time - start_time)
            print('{} cost {} seconds for {} times'.format(func.__name__,
                                                           total_time, number))
            return r
        return _wrapper
    return _deco


def my_load(filename):
    return ImmutableDictOfDict(filename, dtype=np.float32)
    #with open(filename) as f:
    #    o = ujson.load(f)
    #    return o


@timeit()
def test_load(filename):
    return my_load(filename)


@timeit(number=10000)
def my_is_in(o, true_rows, false_rows):
    for r in true_rows:
        assert r in o
    for r in false_rows:
        assert r not in o


def test_in(filename):
    with open(filename) as f:
        ground_truth = ujson.load(f)
        true_rows = prepare_true_key_rows(ground_truth)
        false_rows = prepare_false_key_rows(ground_truth)
        o = my_load(filename)
        my_is_in(o, true_rows, false_rows)


@timeit(number=10000)
def my_len(o):
    return len(o)


def test_len(filename):
    with open(filename) as f:
        ground_truth = ujson.load(f)
        o = my_load(filename)
        assert len(ground_truth.keys()) == my_len(o)


@timeit(number=10000)
def my_iteritems(matrix, rows):
    for item in rows:
        for to_item, simi in matrix[item].iteritems():
            pass


def test_iteritems(filename):
    with open(filename) as f:
        ground_truth = ujson.load(f)
        o = my_load(filename)
        true_rows = prepare_true_key_rows(ground_truth)
        my_iteritems(o, true_rows)


def test():
    o = test_load(JSON_FILE)
    print('new {} bytes'.format(get_memory_usage(o)))
    with open(JSON_FILE) as f:
        o = ujson.load(f)
        print('old {} bytes'.format(get_memory_usage(o)))
    test_in(JSON_FILE)
    test_len(JSON_FILE)
    test_iteritems(JSON_FILE)
    o = my_load(JSON_FILE)
    print(o['1000']['1000'])


def log_mem():
    memory_profiling.log('start')
    o1 = my_load('data/1.json')
    memory_profiling.log('after loading 1')
    o2 = my_load('data/2.json')
    memory_profiling.log('after loading 2')
    o3 = my_load('data/3.json')
    memory_profiling.log('after loading 3')
    o4 = my_load('data/4.json')
    memory_profiling.log('after loading 4')
    gc.collect()
    memory_profiling.log('after gc')

if __name__ == '__main__':
    test()
    #log_mem()
    #print('done')
    #while True:
    #    pass
