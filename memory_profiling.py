# -*- coding: utf-8 -*-

from datetime import datetime
import psutil


LOG_FILE = 'memory.log'
p = psutil.Process()


def log(msg):
    with open(LOG_FILE, 'a') as f:
        now_str = datetime.now().strftime('%X')
        mem = p.memory_full_info()
        mem_str = 'vms:{}, rss:{}, uss:{}'.format(mem.vms, mem.rss, mem.uss)
        log_str = '{},{},{}'.format(now_str, mem_str, msg)
        f.write(log_str+'\n')

