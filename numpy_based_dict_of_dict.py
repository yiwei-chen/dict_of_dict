# -*- coding: utf-8 -*-

"""
{
    "rk1": {
        "ck1": v1,
        "ck2": v2,
    },
    "rk2": {
        "ck1": v3
    }
}

"""

import numpy as np
import ujson


class DictOfDict(object):
    def __init__(self, filename, dtype=float):
        with open(filename) as f:
            original_obj = ujson.load(f)
            self._build_internal_structure(original_obj, dtype)

    def _build_internal_structure(self, dict_of_dict, dtype):
        row_keys = dict_of_dict.keys()
        sorted_row_indices = sorted(range(len(dict_of_dict)),
                                    key=lambda k: hash(row_keys[k]))
        #sorted_row_keys = sorted([hash(k) for k in dict_of_dict.iterkeys()])
        #sorted_row_keys = sorted(dict_of_dict.iterkeys())
        #self.row_keys = np.array(sorted_row_keys)
        self.sorted_hashed_row_keys = np.array(
            [hash(row_keys[i]) for i in sorted_row_indices])
        #self.n_row_keys = len(self.row_keys)
        self.n_row_keys = len(row_keys)
        self.row_end_idx = np.empty(self.n_row_keys, dtype=np.int32)

        n_cell = 0
        for row_dict in dict_of_dict.itervalues():
            n_cell += len(row_dict)
        self.cells = np.empty(n_cell, dtype=dtype)

        col_keys = []
        cell_i = 0
        row_i = 0
        for sorted_row_index in sorted_row_indices:
            row_dict = dict_of_dict[row_keys[sorted_row_index]]
            for col_k, col_v in sorted(row_dict.iteritems(), key=lambda t: t[0]):
                col_keys.append(col_k)
                self.cells[cell_i] = col_v
                cell_i += 1
            self.row_end_idx[row_i] = cell_i
            row_i += 1
        self.col_keys = np.array(col_keys)

    def __contains__(self, item):
        #searched_idx = self.row_keys.searchsorted(item)
        #return searched_idx < self.n_row_keys and self.row_keys[searched_idx] == item
        #searched_idx = self.row_keys.searchsorted(hash(item))
        #return searched_idx < self.n_row_keys and self.row_keys[searched_idx] == hash(item)
        searched_idx = self.sorted_hashed_row_keys.searchsorted(hash(item))
        return (searched_idx < self.n_row_keys
                and self.sorted_hashed_row_keys[searched_idx] == hash(item))

    def __len__(self):
        return self.n_row_keys

    def __getitem__(self, item):
        searched_idx = self.sorted_hashed_row_keys.searchsorted(hash(item))
        if not (searched_idx < self.n_row_keys
                and self.sorted_hashed_row_keys[searched_idx] == hash(item)):
            raise KeyError(item)
        end_cell_idx = self.row_end_idx[searched_idx]
        start_cell_idx = self.row_end_idx[searched_idx-1] if searched_idx != 0 else 0
        selected_col_keys = self.col_keys[start_cell_idx: end_cell_idx]
        selected_col_values = self.cells[start_cell_idx: end_cell_idx]
        return dict(zip(selected_col_keys, selected_col_values))


