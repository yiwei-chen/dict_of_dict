# -*- coding: utf-8 -*-

"""
{
    "rk1": {
        "ck1": v1,
        "ck2": v2,
    },
    "rk2": {
        "ck1": v3
    }
}

"""

import numpy as np
import ujson


class ImmutableDictOfDict(object):
    def __init__(self, filename, dtype=float):
        with open(filename) as f:
            original_obj = ujson.load(f)
            self._build_internal_structure(original_obj, dtype)

    def _build_internal_structure(self, dict_of_dict, dtype):
        n_cell = 0
        for row_dict in dict_of_dict.itervalues():
            n_cell += len(row_dict)
        self.cells = np.empty(n_cell, dtype=dtype)

        self.cell_index_range = {}
        prev_row_end_idx = 0
        col_keys = []
        cell_i = 0
        for row_key, row_dict in dict_of_dict.iteritems():
            row_end_idx = prev_row_end_idx + len(row_dict)
            self.cell_index_range[row_key] = (prev_row_end_idx, row_end_idx)
            prev_row_end_idx = row_end_idx
            for col_k, col_v in row_dict.iteritems():
                col_keys.append(col_k)
                self.cells[cell_i] = col_v
                cell_i += 1
        self.col_keys = np.array(col_keys)
        self.n_row_keys = len(self.cell_index_range)
        del col_keys

    def _build_internal_structure2(self, dict_of_dict, dtype):
        self.cell_index_range = {}
        prev_row_end_idx = 0
        col_keys = []
        cells = []
        for row_key, row_dict in dict_of_dict.iteritems():
            row_end_idx = prev_row_end_idx + len(row_dict)
            self.cell_index_range[row_key] = (prev_row_end_idx, row_end_idx)
            prev_row_end_idx = row_end_idx
            for col_k, col_v in row_dict.iteritems():
                col_keys.append(col_k)
                cells.append(col_v)
        self.col_keys = np.array(col_keys)
        self.cells = np.array(cells, dtype=dtype)
        self.n_row_keys = len(self.cell_index_range)
        del col_keys
        del cells

    def __contains__(self, item):
        return item in self.cell_index_range

    def __len__(self):
        return self.n_row_keys

    def __getitem__(self, item):
        if item not in self:
            raise KeyError(item)
        start_cell_idx, end_cell_idx = self.cell_index_range[item]
        selected_col_keys = self.col_keys[start_cell_idx: end_cell_idx]
        selected_col_values = self.cells[start_cell_idx: end_cell_idx]
        return dict(zip(selected_col_keys, selected_col_values))


