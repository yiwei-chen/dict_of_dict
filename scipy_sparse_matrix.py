import mmh3
import numpy
import pandas as pd
import ujson
from pympler import asizeof
from scipy.sparse import lil_matrix

from util import observe_different_sizes

#o = {'aaaaaaaaaaaaaaaaaa', 'bbbbbbbbbbbbbbbb'}
#observe_different_sizes(o)
#o = {1, 2}
#observe_different_sizes(o)
#o = 1
#observe_different_sizes(o)
#o = '1'
#observe_different_sizes(o)
#o = numpy.array([1])
#observe_different_sizes(o)

#o = {'a':[1], 'b':[1]}
#observe_different_sizes(o)
#
#o = {'a':[1,2], 'b':[1]*10000}
#observe_different_sizes(o)
#
#o = numpy.array([[]], dtype=numpy.int16)
#observe_different_sizes(o)
#
#o = numpy.array([[1,2,3],[4,5,6]], dtype=numpy.int16)
#observe_different_sizes(o)
#
#o = numpy.array([[1,2],[3,4],[5,6]], dtype=numpy.int16)
#observe_different_sizes(o)
#
#o = numpy.array([[1]*200,[2]*200], dtype=numpy.int16)
#observe_different_sizes(o)
#
#o = [1,2,3,4,5,6,7,8,9,0]
#observe_different_sizes(o)
#o = numpy.array([1,2,3,4,5,6,7,8,9,0])
#observe_different_sizes(o)
#
#observe_different_sizes(int(1))
#observe_different_sizes(1.0)
#observe_different_sizes(['1'])
#observe_different_sizes(['1234567890'])
#
#with open('data/1.json') as f:
#    o = ujson.load(f)
#    observe_different_sizes(o)
#    observe_different_sizes(o['1']['1'])
#
#print asizeof.asizeof(['aaaaa'])
#print asizeof.asizeof(numpy.array(['aaaaa']))
#print asizeof.asizeof('aaaaa')


with open('data/1.json') as f:
    o = ujson.load(f)
    row_n = 0
    row_k_size = 0
    col_k_size = 0
    cell_size = 0
    cell_n = 0
    col_max = 0
    for row_k, row_v in o.iteritems():
        row_n += 1
        row_k_size += asizeof.asizeof(row_k)
        cell_n += len(row_v)
        col_max = max(col_max, len(row_v))
        for col_k, col_v in row_v.iteritems():
            col_k_size += asizeof.asizeof(col_k)
            cell_size += asizeof.asizeof(col_v)
    print(row_n)
    print(cell_n)
#    print(row_k_size)
#    print(col_k_size)
#    print(cell_size)
#    print(row_n*asizeof.asizeof('1') + cell_n*(asizeof.asizeof('1')+asizeof.asizeof(1.0)))
#    print(asizeof.asizeof(o))
#    print(col_max)

#with open('data/1.json') as f:
#    o = ujson.load(f)
#    print asizeof.asizeof(o.keys())
#    print asizeof.asizeof(numpy.array(o.keys()))
#    a = numpy.array(o.keys())
#    print a[0]
#    print a[1]

with open('data/1.json') as f:
    o = ujson.load(f)
    col_k = []
    for row_v in o.itervalues():
        col_k.extend(list(row_v.iterkeys()))
    print(len(col_k))
    print(hex(id(col_k[0])))
    print(col_k[0])
    print(hex(id(col_k[1])))
    print(col_k[1])
    print(hex(id(col_k[2])))
    a = numpy.array(col_k)
    print(a.dtype)
    print(hex(id(a[0])))
    print(a[0])
    print(hex(id(a[1])))
    print(a[1])
    print(hex(id(a[2])))
    print(a.nbytes)
